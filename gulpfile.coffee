gulp         = require("gulp")
coffee       = require("gulp-coffee")
stylus       = require("gulp-stylus")
watch        = require("gulp-watch")
livereload   = require("gulp-livereload")
include      = require("gulp-include")
prefix       = require("gulp-autoprefixer")
concat       = require("gulp-concat")
jade         = require("gulp-jade")
addsrc       = require("gulp-add-src")
uglify       = require("gulp-uglify")
csso         = require("gulp-csso")
changed      = require("gulp-changed")
ext_replace  = require("gulp-ext-replace")

path_dev     = "front-dev"
path_dist    = "public"
path_laravel = "resources/views"

files =

	jade:
		watch:          path_dev + "/jade/pages/*.jade"
		watchall:       path_dev + "/jade/**/*.jade"
		src:            path_dev + "/jade/pages/*.jade"
		destLaravel:	path_laravel
		

	stylus:
		watch:          path_dev + "/stylus/**/*.styl"
		src:            path_dev + "/stylus/main*.styl"
		modules:        require('./modules.coffee')(path_dev+"/stylus/coreModules", "styl")
		destDist:       path_dist + "/css"
		

	coffee:
		watch:          path_dev + "/coffee/**/*.coffee"
		src:            path_dev + "/coffee/app.coffee"
		modules:        require('./modules.coffee')(path_dev+"/coffee/coreModules", "coffee")
		modulesCustom:  path_dev + "/coffee/customModules/*.coffee"
		plugins:        require('./modules.coffee')("plugins")
		destDist:       path_dist + "/js"
		


gulp.task "default", ->

	livereload.listen()
	gulp.watch(path_dist + "/css/*.css").on "change", livereload.changed

	gulp.watch files.jade.watch,        [ "build:php" ]
	gulp.watch files.jade.watchall,     [ "build:php-all" ]
	gulp.watch files.stylus.watch,      [ "build:css" ]
	gulp.watch files.coffee.watch,      [ "build:js" ]
	return


gulp.task 'build', [
	'build:php-all'
	'build:js'
	'build:css'
]

gulp.task "build:php", ->
	gulp.src(files.jade.src)
		.pipe(changed(path_dev,extension: ".php"))
		.pipe(jade(pretty: true))
		.pipe(ext_replace('.php'))
		.pipe(gulp.dest(files.jade.destLaravel))
	return

gulp.task "build:php-all", ->
	gulp.src(files.jade.src)
		.pipe(changed(path_dev,extension: ".php"))
		.pipe(jade(pretty: true))
		.pipe(ext_replace('.php'))
		.pipe(gulp.dest(files.jade.destLaravel))
	return

gulp.task "build:css", ->
	gulp.src(files.stylus.src)
		.pipe(addsrc.append(files.stylus.modules))
		.pipe(concat("main.styl"))
		.pipe(stylus({'include css': true}))
		.pipe(prefix())
		.pipe(gulp.dest(files.stylus.destDist))
		.pipe(csso())
	return

gulp.task "build:js", ->
	gulp.src(files.coffee.src)
		.pipe(include())
		.pipe(addsrc.append(files.coffee.modules))
		.pipe(addsrc.append(files.coffee.modulesCustom))
		.pipe(coffee(bare: true))
		.pipe(addsrc.prepend(files.coffee.plugins))
		.pipe(concat("main.js"))
		.pipe(gulp.dest(files.coffee.destDist))
		.pipe(uglify())
	return