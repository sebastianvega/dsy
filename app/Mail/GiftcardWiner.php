<?php

namespace App\Mail;


use App\mails;
use App\Giftcard;
use App\campaigns;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GiftcardWiner extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $email;
    public $giftcard;
    public $campaign;

    public function __construct(mails $email, giftcard $giftcard, campaigns $campaign)
    {
        $this->email = $email;
        $this->giftcard = $giftcard;
        $this->campaign = $campaign;
        $this->subject = "MrPYME";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.send-giftcard');
    }
}
