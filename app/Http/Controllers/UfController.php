<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\data;
use App\usage;
use Maatwebsite\Excel\Facades\Excel;

class UfController extends Controller
{

    public function show(Request $request)
    {
    	$query = 'http://api.sbif.cl/api-sbifv3/recursos_api/uf/'.explode('-',$request['date'])[0].'/'.explode('-',$request['date'])[1].'?apikey=d8093171162117c0c6e8da895b00978d4e2b6a0e&formato=json';
        $json = json_decode(file_get_contents($query), true);
        /*
        $usage = usage::all();
        $last = $usage[count($usage)-1]++;
        $new = new usage;
        $new->id = $last;
        $new->save();

        foreach ($json as $value) {
        	$record = new data;
        	$record->sesion = $last;
        	$record->year = $request['date'])[0];
			$record->month = $request['date'])[1];
			$record->save();
        }*/
    	if (isset($request['download'])) {
    		/*Excel::create('Laravel Excel', function($excel) {
 
	            $excel->sheet('data', function($sheet) {
			 		
	 				$data = data::where('session', $last)->get();
	                $sheet->fromArray($data);
	 
	            	});
        	})->export('xls');*/
    	} else {
	        return $json;
    	}
    }



    public function download(Request $request)
    {
    	$query = 'http://api.sbif.cl/api-sbifv3/recursos_api/uf/'.explode('-',$request['date'])[0].'/'.explode('-',$request['date'])[1].'?apikey=d8093171162117c0c6e8da895b00978d4e2b6a0e&formato=json';
        $json = json_decode(file_get_contents($query), true);

        //return $sbif->getUf($request['date']);
        return $json;
    }


}
