<!DOCTYPE html><!--[if IE 8]>
<html lang="es" class="lt-ie9">
<![endif]--><!--[if gt IE 8]><!--><html lang=es><!--<![endif]--><head><title>Dsy</title><meta charset=UTF-8><meta name=viewport content="width=device-width,initial-scale=1,maximum-scale=1"><meta name=description content="Gestiona tus giftcards."><meta name=csrf-token content="{{csrf_token()}}">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head><body>
	<form method="post" id="form" action="{{URL::to('/')}}/valor-uf">
		<input type="month" name="date">
		<button id="submit" type="submit">Ver en pantalla</button>
		<button id="descargar">Descargar Excel</button>
	</form>
	<div>
		<table>
			<tr>
				<th>fecha</th>
				<th>valor</th>
			</tr>

		</table>
	</div>
	<script type="text/javascript">
		$('#submit').on('click', function(e){
			e.preventDefault()
			var url = $('form').attr('action');
			$.ajax({
		        type:'POST',
		        url: url,
		        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		        data: {date: $('[name="date"]').val()},
		        success:function(data){
		     		for (var i = 0; i < data.UFs.length; i++) {
		     			$('table').append('<tr><td>'+data.UFs[i].Fecha+'</td><td>'+data.UFs[i].Valor+'</td></tr>')
		     		}
		        }
		    });
		})
		$('#descargar').on('click', function(e){
			e.preventDefault()
			var url = $('form').attr('action');
			$.ajax({
		        type:'POST',
		        url: url,
		        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		        data: {date: $('[name="date"]').val(), download: true},
		        success:function(data){
		        	console.log('descargando')
		        }
		    });
		})
	</script>
</body></html>